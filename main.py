import  asyncio, re, datetime, translation
from aiohttp import ClientSession


def td_format(delta: datetime.timedelta, lang):
        hour_count, rem = divmod(delta.seconds, 3600)
        minute_count, second_count = divmod(rem, 60)
        match lang:
            case 'ru':
                return(f'{hour_count}ч, {minute_count}м, {second_count}с')
            case 'en':
                return (f'{hour_count}h, {minute_count}m, {second_count}s')
            case _:
                return (f'{hour_count}h, {minute_count}m, {second_count}s')
class LocalWeather:
    def __init__(self, city, humidity, pressure, temp, wind, day_duration):
        self.city=city
        self.humidity=humidity
        self.pressure=pressure
        self.temp=temp
        self.wind=wind
        self.day_duration=day_duration

    def print(self, translate:bool):
        if translate:
            print(f'Погода в городе {translation.city_name_dict[self.city]}: температура {self.temp}, \n' \
                  f'влажность {self.humidity}, давление {self.pressure}, ветер {self.wind}. \n' \
                  f'Продолжительность дня {td_format(self.day_duration,"ru")}')
                  # f'Продолжительность дня {self.day_duration}')
        else:
            print('******************\n'\
                  f'Weather in {self.city} location: temperature is {self.temp}, \n' \
                  f'humidity is {self.humidity}, pressure is {self.pressure}, wind is {self.wind}. \n' \
                  f'Day duration is {td_format(self.day_duration,"en")}')


#
async def get_weather(city_name):
    _openweatherAPI='0fb610dab7456bc44dbdde2ddba9be71'
    async with ClientSession() as session:
        async with session.get(f'https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={_openweatherAPI}&units=metric') as response:
            response_json = await response.json()
            try:
                humidity=response_json['main']['humidity']
            except:
                humidity='Unable to parse'
            try:
                pressure = response_json['main']['pressure']
            except:
                pressure='Unable to parse'
            try:
                temp = response_json['main']['temp']
            except:
                temp = 'Unable to parse'
            try:
                wind = response_json['wind']["speed"]
            except:
                wind = 'Unable to parse'
            try:
                day_duration=datetime.datetime.fromtimestamp(response_json['sys']['sunset']) - datetime.datetime.fromtimestamp(response_json['sys']['sunrise'])
            except:
                day_duration='Unable to parse'
            weather=LocalWeather(city_name,humidity,pressure,temp,wind,day_duration)
            weather.print(True)
            return weather


def get_cities():
    pattern = '\d{1,2}\) ([a-zA-Z ]*) [-\d]'
    f = open('cities.txt', 'r')
    cities = []
    for data in f:
        re_result = re.match(pattern, data)
        if re_result:
            cities.append(re_result[1])
    return cities

async def main():
    tasks={}
    cities=get_cities()
    for city in cities:
        tasks[city]=asyncio.create_task(get_weather(city))

    for city in cities:
        await tasks[city]

asyncio.run(main())

